package com.ncr.promotions;

import com.ncr.promotions.converters.SapConverterFactory;
import com.ncr.promotions.entities.RetailEvent;
import com.ncr.promotions.handlers.SapPromoSaxHandler;

import javax.xml.parsers.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class PmtUtil {
    //String xml_head_file_name[] = {"ZSP1", "ZGB1", "ZSB1", "ZSC1", "ZSO1", "0101", "0102", "0103", "1002", "ZXB1"};
    String xml_head_file_name[] = {"ZSP1", "ZGB1", "ZSB1", "ZSC1", "ZSO1", "TG01", "TS01", "0101", "0102", "0103", "1002"};

    private boolean fileNameOk(String fileName) {
        boolean flag = false;
        for (int i = 0; i < xml_head_file_name.length; i++) {
            if (fileName.toUpperCase().startsWith(xml_head_file_name[i]) && (fileName.toUpperCase().endsWith(".XML"))) {
                flag = true;
                break;
            }
        }

        return flag;
    }


    public void parsePromotions() {
        Log4JUtil.getLogger(this.getClass()).info("ENTER");

        File currentDirectory = new File(Misc.getInstance().getIncomingPath());
        File files[] = currentDirectory.listFiles();

        Arrays.sort(files);
        Log4JUtil.getLogger(this.getClass()).info("*** list file beg ***");
        for (File f : files) {
            Log4JUtil.getLogger(this.getClass()).info(f.getName());
        }
        Log4JUtil.getLogger(this.getClass()).info("*** list file end ***");


        for (File f : files) {
            if (!(fileNameOk(f.getName()))) {
                Log4JUtil.getLogger(this.getClass()).warn("Discard file >" + f.getName() + "< , it doesn't seem a correct file.");
                continue;
            }

            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

            try {
                Log4JUtil.getLogger(this.getClass()).info(" ");
                Log4JUtil.getLogger(this.getClass()).info(">" + f.getName() + "< reading ...");

                SAXParser xmlPromoParser = saxParserFactory.newSAXParser();
                SapPromoSaxHandler sapPromoSaxHandler = new SapPromoSaxHandler();
                xmlPromoParser.parse(Misc.getInstance().getIncomingPath() + File.separatorChar + f.getName(), sapPromoSaxHandler);
                RetailEvent retailEvent = sapPromoSaxHandler.getRetailEvent();


                if (Misc.getInstance().getStore().equals(retailEvent.getStoreInternalID())) {
                    Misc.getInstance().setCurrentXmlFile(f.getName());
                    new SapConverterFactory().getConverter(retailEvent, f.getName().replaceFirst("[.][^.]+$", "")).convert();
                    Misc.getInstance().createJson(retailEvent, f.getName().replaceFirst("[.][^.]+$", ""));
                } else {
                    Log4JUtil.getLogger(this.getClass()).warn("Not the right Store. Discarding promotion");
                    Misc.getInstance().fileMove(f.getPath(), Misc.getInstance().getErrorPath() + File.separatorChar + f.getName());
                }
            } catch (Exception e) {
                Misc.getInstance().fileMove(f.getPath(), Misc.getInstance().getErrorPath() + File.separatorChar + f.getName());
                Log4JUtil.getLogger(this.getClass()).error(e);
            }
        }

        Log4JUtil.getLogger(this.getClass()).info("EXIT");
    }
}
