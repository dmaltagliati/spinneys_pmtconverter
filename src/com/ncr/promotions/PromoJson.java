package com.ncr.promotions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by User on 21/02/2018.
 */
public class PromoJson{

    String promoNumber;
    String startDate;
    String endDate;
    String pmt;
    List<Association> associations = new ArrayList<>();

    public String getPromoNumber() {
        return promoNumber;
    }

    public void setPromoNumber(String promoNumber) {
        this.promoNumber = promoNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPmt() {
        return pmt;
    }

    public void setPmt(String pmt) {
        this.pmt = pmt;
    }

    public List<Association> getAssociations() {
        return associations;
    }

    public void setAssociations(List<Association> associations) {
        this.associations = associations;
    }
}
