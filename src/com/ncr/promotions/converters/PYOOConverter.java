package com.ncr.promotions.converters;


import com.ncr.promotions.entities.*;
import com.ncr.promotions.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PYOOConverter extends GenericConverter {
    private static final int DISCOUNT_LINE = 0;

    public PYOOConverter(RetailEvent retailEvent, String filename, boolean targeted) throws Exception{
        super (retailEvent, filename, targeted, PromoType.PYOO);
    }

    @Override
    public void convert() {
        List<String> convertedLines = new ArrayList<>();
        String line = getPmtLines().get(DISCOUNT_LINE);
        Offer offer = getRetailEvent().getOffers().get(SINGLE_OFFER);
        Association association;
        String items;
        Product product;

        int index = 1;
        for (ProductGroup productGroup : offer.getGet().getProductGroups()) {
            items = "";
            if ("1".equals(productGroup.getRetailIncentiveOfferDiscountTypeCode())) {
                Log4JUtil.getLogger(PYOOConverter.class).debug("Discarding productGroup: DiscountTypeCode = 1 ");
                continue;
            }
            convertedLines.add(replaceMacro(line, offer, productGroup, index++));

            association = new Association();
            Iterator<Product> productIterator = productGroup.getProducts().iterator();
            while(productIterator.hasNext()){
                product = productIterator.next();
                items += product.getStandardId();
                // TODO: Review logic. Whjat happens if multiple items?
            }
            association.setItem(items);
            association.setPromovar(getRetailEvent().getId() + String.valueOf(index));
            Misc.getInstance().getAssociationList().add(association);
        }
        setPmtLines(convertedLines);
        writePmt();
    }

    private String replaceMacro(String line, Offer offer, ProductGroup productGroup, int index) {
        String lowerBoundary = productGroup.getGrantedQuantityLowerBoundaryDecimalValue();

        line = line.replaceAll("@NUMBER@", getRetailEvent().getId() + String.format("%03d", index));
        line = line.replaceAll("@AMOUNT@", productGroup.getAmount());
        line = line.replaceAll("@QUANTITY@", lowerBoundary);
        line = line.replaceAll("@COMPLEXITY@", "1".equals(lowerBoundary) ? "0" : "1");
        line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(productGroup.getRetailIncentiveOfferDiscountTypeCode()));
        line = line.replaceAll("@ELEMENTID@", buildElementId(productGroup));
        line = line.replaceAll("@PROMOVAR@", getRetailEvent().getId() + String.valueOf(index));
        line = super.replaceMacro(line, offer);

        return line;
    }

    private String buildElementId(ProductGroup productGroup) {
        StringBuilder elementId = new StringBuilder();
        String prefix = "";

        for (Product product : productGroup.getProducts()) {
            elementId.append(prefix).append(product.getStandardId());
            prefix = ";";
        }

        return elementId.toString();
    }
}
