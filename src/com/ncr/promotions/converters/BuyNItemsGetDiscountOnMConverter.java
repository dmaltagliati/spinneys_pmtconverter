package com.ncr.promotions.converters;

import com.ncr.promotions.Misc;
import com.ncr.promotions.entities.*;

import java.util.ArrayList;
import java.util.Iterator;

public class BuyNItemsGetDiscountOnMConverter extends GenericConverter {

    PromoType tipoPromo;
    private static int USER_DEFINED_DATA_LEN = 15;

    public BuyNItemsGetDiscountOnMConverter(RetailEvent retailEvent, String filename, boolean targeted, PromoType tipoPromo) throws Exception {
        //super(retailEvent, filename, targeted, PromoType.BuyNItemsGetDiscountOnM);
        super(retailEvent, filename, targeted, tipoPromo);
        this.tipoPromo = tipoPromo;
    }


    public String replaceMacro(String line, Offer offer) {

        if (tipoPromo == PromoType.BuyNItemsGetDiscountOnMCoupon)
            line = replaceMacroCoupon(line, offer);
        else
            line = replaceMacroGeneric(line, offer);

        line = super.replaceMacro(line, offer);

        return line;
    }

    public String replaceMacroGeneric(String line, Offer offer) {
        ArrayList<String> conditionLineList = new ArrayList<>();
        ArrayList<String> rewardLineList = new ArrayList<>();
        String headerLine = "";
        String conditionLine = "";
        String rewardLine = "";
        String finalRewardLine = line.substring(line.indexOf("@END_COND@") + "@END_COND@".length());
        if (line.indexOf("@BEG_REWARD@") >= 0) {
            headerLine = line.substring(0, line.indexOf("@BEG_REWARD@"));
            rewardLine = line.substring(line.indexOf("@BEG_REWARD@") + "@BEG_REWARD@".length(), line.indexOf("@END_REWARD@"));
            finalRewardLine = line.substring(line.indexOf("@END_REWARD@") + "@END_REWARD@".length());
        }
        if (line.indexOf("@BEG_COND@") >= 0) {
            headerLine = line.substring(0, line.indexOf("@BEG_COND@"));
            conditionLine = line.substring(line.indexOf("@BEG_COND@") + "@BEG_COND@".length(), line.indexOf("@END_COND@"));
        }

        ProductGroup buyProductGroup = offer.getBuy().getProductGroups().get(GenericConverter.SINGLE_GROUP);
        ProductGroup getProductGroup = offer.getGet().getProductGroups().get(GenericConverter.SINGLE_GROUP);
        String lowerBoundary = getProductGroup.getGrantedQuantityLowerBoundaryDecimalValue();
        String requiredQty = buyProductGroup.getRequirementQuantityDecimalValue();
        int reqQty = Integer.parseInt(requiredQty);

        if ("TG01".equals(getRetailEvent().getTypeCode()) || "TS01".equals(getRetailEvent().getTypeCode())) {
            reqQty += Integer.parseInt(lowerBoundary);
        }

        int field = isTargeted() ? 1: 0;
        field += getCurrentRecord() > 1 ? 1 : 0;
        if (conditionLine.length() > 0) {
            for (ProductGroup productGroup : offer.getBuy().getProductGroups()) {
                field++;
                String tmpCondition = new String(conditionLine);
                tmpCondition = tmpCondition.replaceAll("@ELEMENTIDBUY@", buildElementId(productGroup));
                tmpCondition = tmpCondition.replaceAll("@FIELD@", String.valueOf(field));
                conditionLineList.add(tmpCondition + ",");
            }
        }
        if (rewardLine.length() > 0) {
            for (ProductGroup productGroup : offer.getBuy().getProductGroups()) {
                field++;
                String tmpReward = new String(rewardLine);
                tmpReward = tmpReward.replaceAll("@ELEMENTIDBUY@", buildElementId(productGroup));
                tmpReward = tmpReward.replaceAll("@REQUIREDQTYBUY@", String.valueOf(reqQty));
                tmpReward = tmpReward.replaceAll("@FIELD@", String.valueOf(field));
                rewardLineList.add(tmpReward + ",");
            }
        }

        int numField;
        switch (getCurrentRecord()) {
            case 1:
                numField = field + 1;
                break;
            case 2:
                numField = field + 1;
                break;
            default:
                numField = field + 2;
                break;
        }

        //ricostruisco line con : header + tutte le condition + la reward
        StringBuffer finalLine = new StringBuffer(headerLine);

        Iterator conditionListItr = conditionLineList.iterator();
        while (conditionListItr.hasNext()) {
            finalLine.append(conditionListItr.next());
        }
        Iterator rewardListItr = rewardLineList.iterator();
        while (rewardListItr.hasNext()) {
            finalLine.append(rewardListItr.next());
        }

        finalLine.append(finalRewardLine);

        line = finalLine.toString();

        line = line.replaceAll("@AMOUNT@", String.valueOf(Integer.parseInt(getProductGroup.getAmount()) / 100));
        line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(getProductGroup.getRetailIncentiveOfferDiscountTypeCode()));
        line = line.replaceAll("@REQUIREDQTYBUY@", String.valueOf(reqQty));
        line = line.replaceAll("@ELEMENTIDBUY@", buildElementId(buyProductGroup));
        line = line.replaceAll("@ELEMENTIDGET@", buildElementId(getProductGroup));
        line = line.replaceAll("@REWARDQTYGET@", lowerBoundary);
        line = line.replaceAll("@COMPLEXITY@", "1".equals(lowerBoundary) ? "0" : "1");
        line = line.replaceAll("@NUM_FIELDS@", String.valueOf(numField));
        line = line.replaceAll("@FIELD2@", String.valueOf(field + 1)); //rimane solo il field della reward
        line = line.replaceAll("@FIELD3@", String.valueOf(field + 2)); //rimane solo il field della reward
        line = line.replaceAll("@SORTKEY@", Misc.getInstance().isUserDefinedSortKeyByPrice() ? UNIT_PRICE_SORT_KEY : ENTRY_ID_SORT_KEY);
        //line = super.replaceMacro(line, offer);

        return line;
    }

    public String replaceMacroCoupon(String line, Offer offer) {

        ProductGroup buyProductGroup = offer.getBuy().getProductGroups().get(GenericConverter.SINGLE_GROUP);
        ProductGroup getProductGroup = offer.getGet().getProductGroups().get(GenericConverter.SINGLE_GROUP);

        String lowerBoundary = getProductGroup.getGrantedQuantityLowerBoundaryDecimalValue();
        String requiredQty = buyProductGroup.getRequirementQuantityDecimalValue();
        int reqQty = Integer.parseInt(requiredQty);

        //if ("ZXB1".equals(getRetailEvent().getTypeCode())) {
        if ("TG01".equals(getRetailEvent().getTypeCode()) || "TS01".equals(getRetailEvent().getTypeCode())) {
            reqQty += Integer.parseInt(lowerBoundary);
        }

        line = line.replaceAll("@AMOUNT@", getProductGroup.getAmount());
        line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(getProductGroup.getRetailIncentiveOfferDiscountTypeCode()));
        line = line.replaceAll("@REQUIREDQTYBUY@", String.valueOf(reqQty));
        line = line.replaceAll("@ELEMENTIDBUY@", buildElementId(buyProductGroup));
        line = line.replaceAll("@ELEMENTIDGET@", buildElementId(getProductGroup));
        line = line.replaceAll("@REWARDQTYGET@", lowerBoundary);
        line = line.replaceAll("@COMPLEXITY@", "1".equals(lowerBoundary) ? "0" : "1");
        line = line.replaceAll("@SORTKEY@", Misc.getInstance().isUserDefinedSortKeyByPrice() ? UNIT_PRICE_SORT_KEY : ENTRY_ID_SORT_KEY);

        int bufferLen = USER_DEFINED_DATA_LEN - buildElementId(buyProductGroup).length();
        StringBuffer userDefinedData = new StringBuffer(buildElementId(buyProductGroup));

        for (int i = 0; i < bufferLen; i++) {
            userDefinedData.append(" ");
        }
        line = line.replaceAll("@UDD@", userDefinedData.toString());

        //line = super.replaceMacro(line, offer);

        return line;
    }

    private String buildElementId(ProductGroup productGroup) {
        StringBuilder elementId = new StringBuilder();
        String prefix = "";

        for (Product product : productGroup.getProducts()) {
            elementId.append(prefix).append(product.getStandardId());
            prefix = ";";
        }

        return elementId.toString();
    }
}
