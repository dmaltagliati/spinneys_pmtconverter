package com.ncr.promotions.converters;

import com.ncr.promotions.Misc;
import com.ncr.promotions.entities.Offer;
import com.ncr.promotions.entities.ProductGroup;
import com.ncr.promotions.entities.PromoType;
import com.ncr.promotions.entities.RetailEvent;
import com.ncr.promotions.exceptions.TooManyProductsException;

import java.util.Arrays;

public class SapConverterFactory implements ConverterFactory {
    public static final String marketDayTypeCodes[] = {"0101", "0102", "0103", "1002"};
    public static final String targetedTypeCodes[] = {"ZSB1", "TS01"};
    public static final String pyooTypeCodes[] = {"ZSO1"};

    private void checkTooManyProducts(RetailEvent retailEvent) throws TooManyProductsException {
        for (Offer offer : retailEvent.getOffers()) {
            for (ProductGroup productGroup : offer.getGet().getProductGroups()) {
                if (productGroup.getProducts().size() > Misc.getInstance().getMaxProducts()) {
                    throw new TooManyProductsException("Promotion discarded for too many items in get section : " + productGroup.getProducts().size());
                }
            }
            for (ProductGroup productGroup : offer.getBuy().getProductGroups()) {
                if (productGroup.getProducts().size() > Misc.getInstance().getMaxProducts()) {
                    throw new TooManyProductsException("Promotion discarded for too many items in buy section : " + productGroup.getProducts().size());
                }
            }
        }
    }

    @Override
    public MacroConverter getConverter(RetailEvent retailEvent, String filename) throws Exception {
        MacroConverter converter = null;

        checkTooManyProducts(retailEvent);

        if (Arrays.asList(pyooTypeCodes).contains(retailEvent.getTypeCode())) {
            converter = new PYOOConverter(retailEvent, filename, false);
        } else if (Arrays.asList(marketDayTypeCodes).contains(retailEvent.getTypeCode())) {
            converter = new PriceOffsConverter(retailEvent, filename, false);
        } else {
            Offer offer = retailEvent.getOffers().get(GenericConverter.SINGLE_OFFER);
            boolean targeted = Arrays.asList(targetedTypeCodes).contains(retailEvent.getTypeCode());

            if (offer.getBuy().getRequirementMinimumAmount().length() > 0 && offer.getGet().getProductGroups().size() > 0) {
                converter = new SpendXGetYDiscountOnMConverter(retailEvent, filename, targeted);
            } else if (offer.getBuy().getRequirementMinimumAmount().length() > 0) {
                converter = new SpendXGetYDiscountConverter(retailEvent, filename, targeted);

            } else if (offer.getBuy().getProductGroups().size() > 0 && offer.getGet().getAmount().length() > 0) {
                PromoType tipoPromo;
                if (retailEvent.getTypeCode().equals("ZSC1")){
                    tipoPromo = PromoType.BuyNItemsGetYDiscountCoupon;
                }
                else
                    tipoPromo = PromoType.BuyNItemsGetYDiscount;
                converter = new BuyNItemsGetDiscountConverter(retailEvent, filename, targeted, tipoPromo);
            } else if (offer.getBuy().getProductGroups().size() > 0) {
                ProductGroup productGroup = offer.getBuy().getProductGroups().get(GenericConverter.SINGLE_GROUP);

                if (productGroup.getRequirementQuantityDecimalValue().length() > 0) {
                    PromoType tipoPromo;
                    if (retailEvent.getTypeCode().equals("ZSC1")){
                        tipoPromo = PromoType.BuyNItemsGetDiscountOnMCoupon;
                    }
                    else
                        tipoPromo = PromoType.BuyNItemsGetDiscountOnM;

                    converter = new BuyNItemsGetDiscountOnMConverter(retailEvent, filename, targeted, tipoPromo);
                }
            } else if ("O".equals(offer.getGet().getAndOrBusinessRuleExpressionTypeCode())) {
                converter = new GenericDiscountConverter(retailEvent, filename, targeted);
            } else {
                int producGroupLen = retailEvent.getOffers().get(0).getGet().getProductGroups().size();
                PromoType tipoPromo = getMealDealPromoType(producGroupLen);
                converter = new MealDealConverter(retailEvent, filename, targeted, producGroupLen, tipoPromo);
            }
        }

        return converter;
    }

    private PromoType getMealDealPromoType(int producGroupLen) {
        PromoType tipoPromo;
        switch (producGroupLen) {
            case 1:
                tipoPromo = PromoType.MealDeal1;
                break;
            case 2:
                tipoPromo = PromoType.MealDeal2;
                break;
            case 3:
                tipoPromo = PromoType.MealDeal3;
                break;
            case 4:
                tipoPromo = PromoType.MealDeal4;
                break;
            case 5:
                tipoPromo = PromoType.MealDeal5;
                break;
            default:
                tipoPromo = PromoType.Unknown;
                break;
        }
        return tipoPromo;
    }
}
