package com.ncr.promotions.converters;

import com.ncr.promotions.Log4JUtil;
import com.ncr.promotions.Misc;
import com.ncr.promotions.PmtConverter;
import com.ncr.promotions.entities.PromoType;
import com.ncr.promotions.entities.Offer;
import com.ncr.promotions.entities.Product;
import com.ncr.promotions.entities.ProductGroup;
import com.ncr.promotions.entities.RetailEvent;

import java.util.ArrayList;
import java.util.List;

public class PriceOffsConverter extends GenericConverter {
    private static final int DISCOUNT_LINE = 0;

    public PriceOffsConverter(RetailEvent retailEvent, String filename, boolean targeted) throws Exception{
        super(retailEvent, filename, targeted, PromoType.PriceOffs);
    }

    @Override
    public void convert() {
        List<String> convertedLines = new ArrayList<>();

        String line = getPmtLines().get(DISCOUNT_LINE);
        int index = 1;
        for (Offer offer : getRetailEvent().getOffers()) {
            if (! "1".equals(offer.getGet().getRetailIncentiveOfferDiscountTypeCode())) {
                convertedLines.add(replaceMacro(line, offer, index++));
            } else {
                Log4JUtil.getLogger(this.getClass()).warn("Discarding offer: DiscountTypeCode = 1");
            }
        }
        setPmtLines(convertedLines);
        writePmt();
    }

    public String replaceMacro(String line, Offer offer, int index) {
        if (offer.containsScaleProduct()) {
            line = line.replaceAll("@AMOUNT@", "0");
            line = line.replaceAll("@PROMOTIONTYPE@", "1");
            line = line.replaceAll("@REWARDDESCRIPTION@", String.format("%-40s", Misc.getInstance().getScaleProductDescription()));
        }
        line = line.replaceAll("@PRIORITY@", "10");
        line = line.replaceAll("@NUMBER@", getRetailEvent().getId() + String.format("%03d", index));
        line = line.replaceAll("@AMOUNT@", offer.getGet().getAmount());
        line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(offer.getGet().getRetailIncentiveOfferDiscountTypeCode()));
        line = line.replaceAll("@ELEMENTID@", buildElementId(offer));
        line = line.replaceAll("@SORTKEY@", Misc.getInstance().isUserDefinedSortKeyByPrice() ? UNIT_PRICE_SORT_KEY : ENTRY_ID_SORT_KEY);

        line = super.replaceMacro(line, offer);

        return line;
    }

    private String buildElementId(Offer offer) {
        StringBuilder elementId = new StringBuilder();
        String prefix = "";

        for (ProductGroup productGroup : offer.getGet().getProductGroups()) {
            for (Product product : productGroup.getProducts()) {
                elementId.append(prefix).append(product.getStandardId());
                prefix = ";";
            }
        }

        return elementId.toString();
    }
}
