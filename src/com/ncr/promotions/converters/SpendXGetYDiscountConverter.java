package com.ncr.promotions.converters;

import com.ncr.promotions.entities.PromoType;
import com.ncr.promotions.entities.Offer;
import com.ncr.promotions.entities.RetailEvent;

public class SpendXGetYDiscountConverter extends GenericConverter {

    public SpendXGetYDiscountConverter(RetailEvent retailEvent, String filename, boolean targeted) throws Exception{
        super(retailEvent, filename, targeted, PromoType.SpendXGetYDiscount);
    }

    public String replaceMacro(String line, Offer offer) {
        line = line.replaceAll("@AMOUNT@", offer.getGet().getAmount());
        line = line.replaceAll("@REQUIREDAMT@", offer.getBuy().getRequirementMinimumAmount());
        line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(offer.getGet().getRetailIncentiveOfferDiscountTypeCode()));
        line = super.replaceMacro(line, offer);

        return line;
    }
}
