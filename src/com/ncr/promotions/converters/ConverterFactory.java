package com.ncr.promotions.converters;

import com.ncr.promotions.entities.RetailEvent;

public interface ConverterFactory{
    MacroConverter getConverter(RetailEvent retailEvent, String filename) throws Exception;
}
