package com.ncr.promotions.converters;

import com.ncr.promotions.Log4JUtil;
import com.ncr.promotions.Misc;
import com.ncr.promotions.entities.Product;
import com.ncr.promotions.entities.ProductGroup;
import com.ncr.promotions.entities.PromoType;
import com.ncr.promotions.entities.Offer;
import com.ncr.promotions.entities.RetailEvent;

import java.util.ArrayList;
import java.util.List;

public class GenericDiscountConverter extends GenericConverter {
    private static final int DISCOUNT_LINE = 0;

    public GenericDiscountConverter(RetailEvent retailEvent, String filename, boolean targeted) throws Exception{
        super (retailEvent, filename, targeted, PromoType.GenericDiscount);
    }

    @Override
    public void convert() {
        List<String> convertedLines = new ArrayList<>();

        String line = getPmtLines().get(DISCOUNT_LINE);
        Offer offer = getRetailEvent().getOffers().get(SINGLE_OFFER);

        int index = 1;
        for (ProductGroup productGroup : offer.getGet().getProductGroups()) {
            if ("1".equals(productGroup.getRetailIncentiveOfferDiscountTypeCode())) {
                Log4JUtil.getLogger(PYOOConverter.class).debug("Discarding productGroup: DiscountTypeCode = 1");
                continue;
            }
            convertedLines.add(replaceMacro(line, offer, productGroup, index++));
        }
        setPmtLines(convertedLines);
        writePmt();
    }

    private String replaceMacro(String line, Offer offer, ProductGroup productGroup, int index) {
        String lowerBoundary = productGroup.getGrantedQuantityLowerBoundaryDecimalValue();

        line = line.replaceAll("@NUMBER@", getRetailEvent().getId() + String.format("%03d", index));
        line = line.replaceAll("@PROMOVAR@", getRetailEvent().getId());
        line = line.replaceAll("@AMOUNT@", productGroup.getAmount());
        line = line.replaceAll("@QUANTITY@", lowerBoundary);
        line = line.replaceAll("@COMPLEXITY@", "1".equals(lowerBoundary) ? "0" : "1");
        line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(productGroup.getRetailIncentiveOfferDiscountTypeCode()));
        line = line.replaceAll("@ELEMENTID@", buildElementId(productGroup));
        line = super.replaceMacro(line, offer);

        return line;
    }

    private String buildElementId(ProductGroup productGroup) {
        StringBuilder elementId = new StringBuilder();
        String prefix = "";

        for (Product product : productGroup.getProducts()) {
            elementId.append(prefix).append(product.getStandardId());
            prefix = ";";
        }

        return elementId.toString();
    }
}
