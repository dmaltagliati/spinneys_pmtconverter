package com.ncr.promotions.converters;

import com.ncr.promotions.Misc;
import com.ncr.promotions.entities.*;

public class SpendXGetYDiscountOnMConverter extends GenericConverter {

    public SpendXGetYDiscountOnMConverter(RetailEvent retailEvent, String filename, boolean targeted) throws Exception{
        super(retailEvent, filename, targeted, PromoType.SpendXGetDiscountOnM);
    }


    public String replaceMacro(String line, Offer offer) {
        ProductGroup getProductGroup = offer.getGet().getProductGroups().get(GenericConverter.SINGLE_GROUP);
        String lowerBoundary = getProductGroup.getGrantedQuantityLowerBoundaryDecimalValue();


        line = line.replaceAll("@AMOUNT@", offer.getGet().getProductGroups().get(GenericConverter.SINGLE_GROUP).getAmount());
        line = line.replaceAll("@REQUIREDAMT@", offer.getBuy().getRequirementMinimumAmount());
        line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(offer.getGet().getProductGroups().get(GenericConverter.SINGLE_GROUP).getRetailIncentiveOfferDiscountTypeCode()));
        line = line.replaceAll("@ELEMENTIDGET@", buildElementId(getProductGroup));
        line = line.replaceAll("@REWARDQTYGET@", lowerBoundary);
        line = line.replaceAll("@COMPLEXITY@", "1".equals(lowerBoundary) ? "0" : "1");
        line = line.replaceAll("@SORTKEY@", Misc.getInstance().isUserDefinedSortKeyByPrice() ? UNIT_PRICE_SORT_KEY : ENTRY_ID_SORT_KEY);
        line = super.replaceMacro(line, offer);

        return line;
    }

    private String buildElementId(ProductGroup productGroup) {
        StringBuilder elementId = new StringBuilder();
        String prefix = "";

        for (Product product : productGroup.getProducts()) {
            elementId.append(prefix).append(product.getStandardId());
            prefix = ";";
        }

        return elementId.toString();
    }

}
