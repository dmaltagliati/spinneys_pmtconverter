package com.ncr.promotions.converters;

import com.ncr.promotions.entities.*;

import java.util.ArrayList;
import java.util.Iterator;

public class BuyNItemsGetDiscountConverter extends GenericConverter {

    PromoType tipoPromo;
    private static int USER_DEFINED_DATA_LEN = 15;

    public BuyNItemsGetDiscountConverter(RetailEvent retailEvent, String filename, boolean targeted, PromoType tipoPromo) throws Exception {
        //super(retailEvent, filename, targeted, PromoType.BuyNItemsGetYDiscount);
        super(retailEvent, filename, targeted, tipoPromo);
        this.tipoPromo = tipoPromo;
    }

    public String replaceMacro(String line, Offer offer) {

        if (tipoPromo == PromoType.BuyNItemsGetYDiscountCoupon)
            line = replaceMacroCoupon(line, offer);
        else
            line = replaceMacroGeneric(line, offer);

        line = super.replaceMacro(line, offer);

        return line;
    }

    private String replaceMacroGeneric(String line, Offer offer) {
        ArrayList<String> conditionLineList = new ArrayList<>();
        String headerLine = line.substring(0, line.indexOf("@BEG_COND@"));
        String conditionLine = line.substring(line.indexOf("@BEG_COND@") + "@BEG_COND@".length(), line.indexOf("@END_COND@"));
        String rewardLine = line.substring(line.indexOf("@BEG_REWARD@") + "@BEG_REWARD@".length(), line.indexOf("@END_REWARD@"));

        ProductGroup buyProductGroup = offer.getBuy().getProductGroups().get(GenericConverter.SINGLE_GROUP);

        Iterator productGroupsItr = offer.getBuy().getProductGroups().iterator();
        ProductGroup tmpGroup;
        int field = isTargeted() ? 1: 0;
        while (productGroupsItr.hasNext()) {
            field++;
            String tmpCondition = new String(conditionLine);
            tmpGroup = (ProductGroup) productGroupsItr.next();
            tmpCondition = tmpCondition.replaceAll("@ELEMENTIDBUY@", buildElementId(tmpGroup));
            tmpCondition = tmpCondition.replaceAll("@FIELD@", String.valueOf(field));
            conditionLineList.add(tmpCondition + ",");
        }

        int numField = field + 1; //abbiamo tante condition e 1 sola reward

        //ricostruisco line con : header + tutte le condition + la reward
        StringBuffer finalLine = new StringBuffer(headerLine);

        Iterator conditionListItr = conditionLineList.iterator();
        while (conditionListItr.hasNext()) {
            finalLine.append(conditionListItr.next());
        }

        finalLine.append(rewardLine);

        line = finalLine.toString();


        line = line.replaceAll("@REQUIREDQTYBUY@", buyProductGroup.getRequirementQuantityDecimalValue());
        line = line.replaceAll("@ELEMENTIDBUY@", buildElementId(buyProductGroup));
        line = line.replaceAll("@AMOUNT@", offer.getGet().getAmount());
        line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(offer.getGet().getRetailIncentiveOfferDiscountTypeCode()));
        line = line.replaceAll("@NUM_FIELDS@", String.valueOf(numField));
        line = line.replaceAll("@FIELD@", String.valueOf(numField)); //rimane solo il field della reward

        //line = super.replaceMacro(line, offer);

        return line;
    }

    private String replaceMacroCoupon(String line, Offer offer) {

        ProductGroup buyProductGroup = offer.getBuy().getProductGroups().get(GenericConverter.SINGLE_GROUP);

        line = line.replaceAll("@REQUIREDQTYBUY@", buyProductGroup.getRequirementQuantityDecimalValue());
        line = line.replaceAll("@ELEMENTIDBUY@", buildElementId(buyProductGroup));
        line = line.replaceAll("@AMOUNT@", offer.getGet().getAmount());
        line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(offer.getGet().getRetailIncentiveOfferDiscountTypeCode()));


        int bufferLen = USER_DEFINED_DATA_LEN - buildElementId(buyProductGroup).length();
        StringBuffer userDefinedData = new StringBuffer(buildElementId(buyProductGroup));

        for (int i = 0; i < bufferLen; i++) {
            userDefinedData.append(" ");
        }
        line = line.replaceAll("@UDD@", userDefinedData.toString());


        //line = super.replaceMacro(line, offer);

        return line;
    }

    private String buildElementId(ProductGroup productGroup) {
        StringBuilder elementId = new StringBuilder();
        String prefix = "";

        for (Product product : productGroup.getProducts()) {
            elementId.append(prefix).append(product.getStandardId());
            prefix = ";";
        }

        return elementId.toString();
    }
}
