package com.ncr.promotions.converters;

import com.ncr.promotions.Log4JUtil;
import com.ncr.promotions.Misc;
import com.ncr.promotions.entities.Offer;
import com.ncr.promotions.entities.ProductGroup;
import com.ncr.promotions.entities.PromoType;
import com.ncr.promotions.entities.RetailEvent;
import com.ncr.promotions.exceptions.TemplateNotFoundException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class GenericConverter implements MacroConverter {
    private PromoType promoType;
    private List<String> pmtLines = new ArrayList<>();
    private RetailEvent retailEvent;
    private String filename;
    private boolean targeted;
    private int currentRecord = 0;

    public static final int SINGLE_GROUP = 0;
    public static final int SINGLE_OFFER = 0;
    public static final String TEMPLATE_EXT = ".template";
    public static final String PMT_EXT = ".pmt";
    public static final String UNIT_PRICE_SORT_KEY = "1";
    public static final String ENTRY_ID_SORT_KEY = "4";

    public GenericConverter(RetailEvent retailEvent, String filename, boolean targeted, PromoType promotype) throws Exception{
        this.retailEvent = retailEvent;
        this.filename = filename + PMT_EXT;
        this.targeted = targeted;
        this.promoType = promotype;

        loadTemplate();
    }

    public void loadTemplate() throws Exception{
        String filename = promoType.name() + (targeted ? "_targeted" : "");
        Log4JUtil.getLogger(this.getClass()).info("Loading promo : " + filename);
        try {
            String line;
            BufferedReader reader = new BufferedReader(new FileReader(Misc.getInstance().getTemplatePath() + File.separatorChar + filename + TEMPLATE_EXT));
            while ((line = reader.readLine()) != null) {
                pmtLines.add(line);
            }
        } catch (Exception e) {
            throw new TemplateNotFoundException(e.getMessage());
        }
    }

    private boolean isDiscounted(Offer offer) {
        if ("1".equals(offer.getGet().getRetailIncentiveOfferDiscountTypeCode())) {
            return false;
        } else {
            List<ProductGroup> productGroups = offer.getGet().getProductGroups();
            if (productGroups.size() > 0 ) {
                if ("1".equals(productGroups.get(GenericConverter.SINGLE_GROUP).getRetailIncentiveOfferDiscountTypeCode())){
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void convert() {
        List<String> convertedLines = new ArrayList<>();
        Offer offer = getRetailEvent().getOffers().get(SINGLE_OFFER);
        if (!isDiscounted(offer)) {
            Log4JUtil.getLogger(this.getClass()).warn("Discarding offer: DiscountTypeCode = 1");
        } else {
            for (String line : getPmtLines()) {
                currentRecord++;
                convertedLines.add(replaceMacro(line, offer));
            }
            setPmtLines(convertedLines);
            writePmt();
        }
    }

    public String replaceMacro(String line, Offer offer) {
        line = line.replaceAll("@DESCRIPTION@", getRetailEvent().getDescription());
        line = line.replaceAll("@STARTDATE@", getRetailEvent().getSalesPeriod().getStartDate());
        line = line.replaceAll("@ENDDATE@", getRetailEvent().getSalesPeriod().getEndDate());
        line = line.replaceAll("@MEMBER@", "0");
        line = line.replaceAll("@PRIORITY@", "1");
        line = line.replaceAll("@BEG_REWARD@", "");
        line = line.replaceAll("@END_REWARD@", "");
        line = line.replaceAll("@NUMBER@", getRetailEvent().getId());
        line = line.replaceAll("@REWARDDESCRIPTION@", String.format("%-40s", getRetailEvent().getDescription()));
        line = line.replaceAll("@DISCOUNTLIMIT@", Misc.getInstance().getDiscountLimitValue());
        line = line.replaceAll("@PROMOVARLIMIT@", Misc.getInstance().getDiscountLimitPromovar());

        return line;
    }

    public PromoType getPromoType() {
        return promoType;
    }

    public void setPromoType(PromoType promoType) {
        this.promoType = promoType;
    }

    public List<String> getPmtLines() {
        return pmtLines;
    }

    public void setPmtLines(List<String> pmtLines) {
        this.pmtLines = pmtLines;
    }

    public RetailEvent getRetailEvent() {
        return retailEvent;
    }

    public void setRetailEvent(RetailEvent retailEvent) {
        this.retailEvent = retailEvent;
    }

    public boolean isTargeted() {
        return targeted;
    }

    public void setTargeted(boolean targeted) {
        this.targeted = targeted;
    }

    public String getDiscountType(String value) {
        String type = "";

        switch (value) {
            case "1": //no discount
                break;
            case "2": //discount price (sell at price)
                type = "3";
                break;
            case "3": //discount amount
                type = "0";
                break;
            case "4": //discount percentage
                type = "1";
                break;
        }
        return type;
    }

    public void writePmt() {
        Log4JUtil.getLogger(this.getClass()).info("ENTER writePmt. filename: " + filename);
        String path = Misc.getInstance().getPmtPath();
        String currentXmlFile = Misc.getInstance().getCurrentXmlFile();

        File file = new File(path + File.separatorChar + filename);
        FileWriter fw = null;
        try {
            fw = new FileWriter(file);

            for (String line : pmtLines) {
                Log4JUtil.getLogger(this.getClass()).info("writing line: >" + line + "<");
                fw.write(line + "\r\n");
                Misc.getInstance().setWholePmt(line + "\r\n");
            }
        } catch (IOException e) {
            Misc.getInstance().fileMove(Misc.getInstance().getIncomingPath()+ File.separatorChar +  currentXmlFile, Misc.getInstance().getErrorPath() + File.separatorChar + currentXmlFile);
            e.printStackTrace();
            //TODO: Better handling
        } finally {
            try {
                fw.flush();
                fw.close();
                Misc.getInstance().fileMove(Misc.getInstance().getIncomingPath()+ File.separatorChar +  currentXmlFile, Misc.getInstance().getProcessedPath() + File.separatorChar + currentXmlFile);
            } catch (IOException eInner) {
                Misc.getInstance().fileMove(Misc.getInstance().getIncomingPath()+ File.separatorChar +  currentXmlFile, Misc.getInstance().getErrorPath() + File.separatorChar + currentXmlFile);
                eInner.printStackTrace();
                //TODO: Better handling
            }
        }

        Log4JUtil.getLogger(this.getClass()).info("EXIT writePmt");
    }

    public int getCurrentRecord() {
        return currentRecord;
    }

    public void setCurrentRecord(int currentRecord) {
        this.currentRecord = currentRecord;
    }
}
