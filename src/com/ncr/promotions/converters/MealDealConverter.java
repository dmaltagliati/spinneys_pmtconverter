package com.ncr.promotions.converters;

import com.ncr.promotions.Log4JUtil;
import com.ncr.promotions.entities.Product;
import com.ncr.promotions.entities.PromoType;
import com.ncr.promotions.entities.Offer;
import com.ncr.promotions.entities.ProductGroup;
import com.ncr.promotions.entities.RetailEvent;

public class MealDealConverter extends GenericConverter {

    int productGroupLen;

    public MealDealConverter(RetailEvent retailEvent, String filename, boolean targeted, int productGroupLen, PromoType tipoPromo) throws Exception{
        super(retailEvent, filename, targeted, tipoPromo);
        this.productGroupLen = productGroupLen;
    }

    public String replaceMacro(String line, Offer offer) {
        line = line.replaceAll("@PRIORITY@", "5");
        switch(productGroupLen){
            case 1:
                line = line.replaceAll("@AMOUNT@", offer.getGet().getProductGroups().get(GenericConverter.SINGLE_GROUP).getAmount());
                line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(offer.getGet().getProductGroups().get(GenericConverter.SINGLE_GROUP).getRetailIncentiveOfferDiscountTypeCode()));
                line = line.replaceAll("@QTYTOBUY@", offer.getGet().getProductGroups().get(GenericConverter.SINGLE_GROUP).getGrantedQuantityLowerBoundaryDecimalValue());
                line = line.replaceAll("@ELEMENTID@", buildElementId(offer, 0));
                break;
            case 5:
                line = line.replaceAll("@ELEMENTID5@", buildElementId(offer, 4));
                line = line.replaceAll("@QTYTOBUY5@", offer.getGet().getProductGroups().get(4).getGrantedQuantityLowerBoundaryDecimalValue());
            case 4:
                line = line.replaceAll("@ELEMENTID4@", buildElementId(offer, 3));
                line = line.replaceAll("@QTYTOBUY4@", offer.getGet().getProductGroups().get(3).getGrantedQuantityLowerBoundaryDecimalValue());
            case 3:
                line = line.replaceAll("@ELEMENTID3@", buildElementId(offer, 2));
                line = line.replaceAll("@QTYTOBUY3@", offer.getGet().getProductGroups().get(2).getGrantedQuantityLowerBoundaryDecimalValue());
            case 2:
                line = line.replaceAll("@AMOUNT@", offer.getGet().getAmount());
                line = line.replaceAll("@PROMOTIONTYPE@", getDiscountType(offer.getGet().getRetailIncentiveOfferDiscountTypeCode()));
                line = line.replaceAll("@ELEMENTID@", buildElementId(offer, 0));
                line = line.replaceAll("@ELEMENTID2@", buildElementId(offer, 1));
                line = line.replaceAll("@QTYTOBUY@", offer.getGet().getProductGroups().get(0).getGrantedQuantityLowerBoundaryDecimalValue());
                line = line.replaceAll("@QTYTOBUY2@", offer.getGet().getProductGroups().get(1).getGrantedQuantityLowerBoundaryDecimalValue());
                break;
            default:
                break;
        }
        line = super.replaceMacro(line, offer);

        return line;
    }

    private String buildElementId(Offer offer, int index) {
        StringBuilder elementId = new StringBuilder();
        String prefix = "";

        ProductGroup productGroup = offer.getGet().getProductGroups().get(index);
        for (Product product : productGroup.getProducts()) {
            elementId.append(prefix).append(product.getStandardId());
            prefix = ";";
        }

        return elementId.toString();
    }
}
