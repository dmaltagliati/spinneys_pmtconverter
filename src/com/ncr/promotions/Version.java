package com.ncr.promotions;

/**
 * Created by daniele on 13/07/2017.
 */
public class Version {
    private final static String NAME = "PMTConverter";
    private final static String VERSION = "1.2.";
    private final static String REVISION = "3";
    private final static String DATE = "08-05-2019";


    public static String getDate() {
        return DATE;
    }

    public static String getVersion() {
        return VERSION;
    }

    public static String getName() {
        return NAME;
    }

    public static String getRevision() {
        return REVISION;
    }


}
