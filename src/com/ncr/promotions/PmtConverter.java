package com.ncr.promotions;

public class PmtConverter {

    public static void main(String[] args) {

        if (!Log4JUtil.init()){
            System.out.println(" log4jInit failed... exit program");
            System.exit(98);
        }

        Log4JUtil.getLogger(PmtConverter.class).info("  ");
        Log4JUtil.getLogger(PmtConverter.class).info("  ");
        Log4JUtil.getLogger(PmtConverter.class).info("*******************************************");
        Log4JUtil.getLogger(PmtConverter.class).info(" START: " + Version.getName() + " - " + Version.getVersion()  + Version.getRevision() + " - " + Version.getDate());
        Log4JUtil.getLogger(PmtConverter.class).info("*******************************************");

        if (!Misc.getInstance().loadMainParameters()){
            System.out.println(" loadMainParameters failed... exit program");
            System.exit(99);
        }

        PmtUtil pmt = new PmtUtil();
        try {
            pmt.parsePromotions();
        }catch (Exception e){
            //TODO: Handle this better
        }
    }

}
