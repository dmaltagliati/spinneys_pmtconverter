package com.ncr.promotions.exceptions;

/**
 * Created by User on 22/02/2018.
 */
public class TemplateNotFoundException extends Exception{

    public TemplateNotFoundException(){}

    public TemplateNotFoundException(String description){
        super(description);
    }
}
