package com.ncr.promotions.exceptions;

/**
 * Created by User on 04/12/2017.
 */
public class MacroNotMappedException  extends Exception{
    public MacroNotMappedException(){

    }

    public MacroNotMappedException(String description){
        super (description);
    }
}
