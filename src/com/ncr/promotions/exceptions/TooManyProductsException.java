package com.ncr.promotions.exceptions;

/**
 * Created by User on 22/02/2018.
 */
public class TooManyProductsException extends Exception{
    public TooManyProductsException(){}

    public TooManyProductsException(String description){
        super(description);
    }
}
