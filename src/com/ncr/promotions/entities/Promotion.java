package com.ncr.promotions.entities;

import com.ncr.promotions.Log4JUtil;

import java.util.*;

public class Promotion {
    private static String ID = "id";
    private static String VERSION = "version";

    private ArrayList<PromoField> fields;
    private PromoType type; //type of promotion: deal&meal, priceOffer...
    private String pmtName;
    private String member;

    public Promotion(){
     fields = new ArrayList<PromoField>();
    }

    public void log(){

        Log4JUtil.getLogger(this.getClass()).info("  ");
        Log4JUtil.getLogger(this.getClass()).info("*** BEGIN PROMOTION +++");

        Iterator<PromoField> promoIterator = fields.iterator();
        PromoField field ;
        PromoField fieldOffer;

        while(promoIterator.hasNext()){
            field = promoIterator.next();
            if (field.getKey().equals("<Offer>")){
                Log4JUtil.getLogger(this.getClass()).info( "BEG OFFER");
                Promotion offer = (Promotion)field.getValue();
                Iterator<PromoField> offerIterator = offer.getFields().iterator();
                while(offerIterator.hasNext()){
                    fieldOffer = offerIterator.next();
                    Log4JUtil.getLogger(this.getClass()).info( fieldOffer.getKey()+": " + fieldOffer.getValue());
                }
                Log4JUtil.getLogger(this.getClass()).info( "END OFFER");
                continue;
            }
            Log4JUtil.getLogger(this.getClass()).info( field.getKey()+": " + field.getValue());
        }
        Log4JUtil.getLogger(this.getClass()).info("PMT name: " + this.pmtName);
        Log4JUtil.getLogger(this.getClass()).info("*** END PROMOTION +++");
    }

    public ArrayList<PromoField> getFields() {
        return fields;
    }

    public PromoType getType() {
        return type;
    }

    public void setType(PromoType type) {
        this.type = type;
    }

    public String getPmtName() {
        return pmtName;
    }

    public void setPmtName(String pmtName) {
        this.pmtName = pmtName;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }
}
