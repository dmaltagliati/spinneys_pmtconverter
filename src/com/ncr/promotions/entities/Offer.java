package com.ncr.promotions.entities;

public class Offer {
    private String description = "";
    private String store = "";
    private String activationStatus = "";
    private String categoryCode = "";
    private String targetTypeCode = "";
    private Buy buy;
    private Get get;

    public Offer() {
        buy = new Buy();
        get = new Get();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getActivationStatus() {
        return activationStatus;
    }

    public void setActivationStatus(String activationStatus) {
        this.activationStatus = activationStatus;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getTargetTypeCode() {
        return targetTypeCode;
    }

    public void setTargetTypeCode(String targetTypeCode) {
        this.targetTypeCode = targetTypeCode;
    }

    public Buy getBuy() {
        return buy;
    }

    public void setBuy(Buy buy) {
        this.buy = buy;
    }

    public Get getGet() {
        return get;
    }

    public void setGet(Get get) {
        this.get = get;
    }


    public boolean containsScaleProduct() {
        for (ProductGroup productGroup : get.getProductGroups()) {
            for (Product product : productGroup.getProducts()) {
                if (product.isScale()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "description='" + description + '\'' +
                ", store='" + store + '\'' +
                ", activationStatus='" + activationStatus + '\'' +
                ", categoryCode='" + categoryCode + '\'' +
                ", targetTypeCode='" + targetTypeCode + '\'' +
                ", buy=" + buy +
                ", get=" + get +
                '}';
    }
}
