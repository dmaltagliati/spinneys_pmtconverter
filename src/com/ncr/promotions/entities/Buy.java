package com.ncr.promotions.entities;

public class Buy extends Element {
    private String requirementMinimumAmount = "";

    public Buy() {
        super();
    }

    public String getRequirementMinimumAmount() {
        return requirementMinimumAmount;
    }

    public void setRequirementMinimumAmount(String requirementMinimumAmount) {
        this.requirementMinimumAmount = requirementMinimumAmount;
    }

    @Override
    public String toString() {
        return "Buy{" +
                "requirementMinimumAmount='" + requirementMinimumAmount + '\'' +
                "} " + super.toString();
    }
}
