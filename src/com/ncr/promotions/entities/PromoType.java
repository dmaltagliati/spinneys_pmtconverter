package com.ncr.promotions.entities;

/**
 * Created by User on 15/02/2018.
 */
public enum PromoType {
    PriceOffs,
    MealDeal,
    MealDeal1,
    MealDeal2,
    MealDeal3,
    MealDeal4,
    MealDeal5,
    GenericDiscount,
    BuyNItemsGetDiscountOnM,
    BuyNItemsGetYDiscount,
    SpendXGetDiscountOnM,
    SpendXGetYDiscount,
    PYOO,
    BuyNItemsGetDiscountOnMCoupon,
    BuyNItemsGetYDiscountCoupon,
    Unknown
}

