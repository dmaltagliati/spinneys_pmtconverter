package com.ncr.promotions.entities;

import java.util.ArrayList;
import java.util.List;

public class ProductGroup {
    private List<Product> products;
    private String grantedQuantityLowerBoundaryDecimalValue = "";
    private String retailBonusBuyProductSelectionConditionCode = "";
    private String retailIncentiveOfferDiscountTypeCode = "";
    private String amount = "";
    private String requirementQuantityDecimalValue = "";

    public ProductGroup() {
        products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getGrantedQuantityLowerBoundaryDecimalValue() {
        return grantedQuantityLowerBoundaryDecimalValue;
    }

    public void setGrantedQuantityLowerBoundaryDecimalValue(String grantedQuantityLowerBoundaryDecimalValue) {
        this.grantedQuantityLowerBoundaryDecimalValue = grantedQuantityLowerBoundaryDecimalValue;
    }

    public String getRetailBonusBuyProductSelectionConditionCode() {
        return retailBonusBuyProductSelectionConditionCode;
    }

    public void setRetailBonusBuyProductSelectionConditionCode(String retailBonusBuyProductSelectionConditionCode) {
        this.retailBonusBuyProductSelectionConditionCode = retailBonusBuyProductSelectionConditionCode;
    }

    public String getRetailIncentiveOfferDiscountTypeCode() {
        return retailIncentiveOfferDiscountTypeCode;
    }

    public void setRetailIncentiveOfferDiscountTypeCode(String retailIncentiveOfferDiscountTypeCode) {
        this.retailIncentiveOfferDiscountTypeCode = retailIncentiveOfferDiscountTypeCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "ProductGroup{" +
                "products=" + products +
                ", grantedQuantityLowerBoundaryDecimalValue='" + grantedQuantityLowerBoundaryDecimalValue + '\'' +
                ", retailBonusBuyProductSelectionConditionCode='" + retailBonusBuyProductSelectionConditionCode + '\'' +
                ", retailIncentiveOfferDiscountTypeCode='" + retailIncentiveOfferDiscountTypeCode + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }

    public void setRequirementQuantityDecimalValue(String requirementQuantityDecimalValue) {
        this.requirementQuantityDecimalValue = requirementQuantityDecimalValue;
    }

    public String getRequirementQuantityDecimalValue() {
        return requirementQuantityDecimalValue;
    }
}
