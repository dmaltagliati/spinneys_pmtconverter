package com.ncr.promotions.entities;

public class Product {
    private String standardId = "";
    private boolean scale = false;

    public Product(String standardId) {
        this.standardId = standardId;
    }

    public String getStandardId() {
        return standardId;
    }

    public void setStandardId(String standardId) {
        this.standardId = standardId;
    }

    public boolean isScale() {
        return scale;
    }

    public void setScale(boolean scale) {
        this.scale = scale;
    }

    @Override
    public String toString() {
        return "Product{" +
                "standardId='" + standardId + '\'' +
                ", scale=" + scale +
                '}';
    }
}
