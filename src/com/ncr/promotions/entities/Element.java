package com.ncr.promotions.entities;

import java.util.ArrayList;
import java.util.List;

public class Element {
    private String andOrBusinessRuleExpressionTypeCode = "";
    private List<ProductGroup> productGroups;

    public Element() {
        productGroups = new ArrayList<>();
    }

    public String getAndOrBusinessRuleExpressionTypeCode() {
        return andOrBusinessRuleExpressionTypeCode;
    }

    public void setAndOrBusinessRuleExpressionTypeCode(String andOrBusinessRuleExpressionTypeCode) {
        this.andOrBusinessRuleExpressionTypeCode = andOrBusinessRuleExpressionTypeCode;
    }

    public List<ProductGroup> getProductGroups() {
        return productGroups;
    }

    public void setProductGroups(List<ProductGroup> productGroups) {
        this.productGroups = productGroups;
    }


}
