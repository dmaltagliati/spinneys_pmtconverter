package com.ncr.promotions.entities;

import java.util.ArrayList;
import java.util.List;

public class RetailEvent {
    private String id = "";
    private String typeCode = "";
    private SalesPeriod salesPeriod;
    private String storeInternalID = "";
    private String description = "";
    private List<Offer> offers;

    public RetailEvent() {
        salesPeriod = new SalesPeriod();
        offers = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public SalesPeriod getSalesPeriod() {
        return salesPeriod;
    }

    public void setSalesPeriod(SalesPeriod salesPeriod) {
        this.salesPeriod = salesPeriod;
    }

    public String getStoreInternalID() {
        return storeInternalID;
    }

    public void setStoreInternalID(String storeInternalID) {
        this.storeInternalID = storeInternalID;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "RetailEvent{" +
                "id='" + id + '\'' +
                ", typeCode='" + typeCode + '\'' +
                ", salesPeriod=" + salesPeriod +
                ", storeInternalID='" + storeInternalID + '\'' +
                ", description='" + description + '\'' +
                ", offers=" + offers +
                '}';
    }
}
