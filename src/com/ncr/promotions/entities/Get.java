package com.ncr.promotions.entities;

public class Get extends Element {
    private String retailIncentiveOfferDiscountTypeCode = "";
    private String amount = "";

    public Get() {
        super();
    }

    public String getRetailIncentiveOfferDiscountTypeCode() {
        return retailIncentiveOfferDiscountTypeCode;
    }

    public void setRetailIncentiveOfferDiscountTypeCode(String retailIncentiveOfferDiscountTypeCode) {
        this.retailIncentiveOfferDiscountTypeCode = retailIncentiveOfferDiscountTypeCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Get{" +
                "retailIncentiveOfferDiscountTypeCode='" + retailIncentiveOfferDiscountTypeCode + '\'' +
                ", amount='" + amount + '\'' +
                "} " + super.toString();
    }
}
