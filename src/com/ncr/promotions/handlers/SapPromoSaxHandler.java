package com.ncr.promotions.handlers;

import com.ncr.promotions.Misc;
import com.ncr.promotions.entities.*;
import com.ncr.promotions.Log4JUtil;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class SapPromoSaxHandler extends DefaultHandler{
    private String theTag;
    private RetailEvent retailEvent;
    private Offer offer;
    private ProductGroup productGroup;
    private Product currentProduct;

    private boolean isRetailEvent;
    private boolean isId;
    private boolean isTypeCode;
    private boolean isSalesPeriod;
    private boolean isStartDate;
    private boolean isEndDate;
    private boolean isCategoryCode;
    private boolean isSalesArea;
    private boolean isDescription;
    private boolean isStoreNumber;
    private boolean isActivationStatusCode;
    private boolean isRetailIncentiveOfferDiscountTypeCode;
    private boolean isAndOrGet;
    private boolean isPriceAmt;
    private boolean isStandardID;
    private boolean isSeasonYearId;
    private boolean isRequirementMinimumAmount;
    private boolean isGrantedQuantityLowerBoundaryDecimalValue;
    private boolean isBuy;
    private boolean isGet;
    private boolean isRequirementQuantityDecimalValue;
    private boolean isOffer;
    private boolean isProductGroup;
    private boolean isRetailBonusBuyProductSelectionConditionCode;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrib) throws SAXException {
        Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("");
        Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("ENTER startElement  <" + qName+">");
        theTag = qName.trim();

        try {
            switch (theTag.toLowerCase()){
                case "retailevent":
                    retailEvent = new RetailEvent();
                    isRetailEvent = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <RetailEvent> - isRetailEvent: " + isRetailEvent);
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin RetailEvent - startEvent: " + retailEvent);
                    break;
                case "id":
                    if (isRetailEvent) {
                        isId = true;
                        Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin ID - " + isId);
                    }
                    if (!isId){
                        Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("This is not a RetailEvent ID field");
                    }
                    break;
                case "typecode":
                    isTypeCode = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <TypeCode> - isTypeCode: " + isTypeCode);
                    break;
                case "salesperiod":
                    if (!isSalesArea) {
                        isSalesPeriod = true;
                        retailEvent.setSalesPeriod(new SalesPeriod());
                        Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <SalesPeriod> - isSalesPeriod: " + isSalesPeriod);
                    } else {
                        Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("This is not the right <SalesPeriod> - isSalesPeriod: " + isSalesPeriod);
                    }
                    break;
                case "startdate":
                    isStartDate = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <StartDate> - isStartDate: " + isStartDate);
                    break;
                case "enddate":
                    isEndDate = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <EndDate> - isEndDate: " + isEndDate);
                    break;
                case "salesarea":
                    isSalesArea = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <SalesArea> - isSalesArea: " + isSalesArea);
                    break;
                case "description":
                    isDescription = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <Description> - isDescription: " + isDescription);
                    break;
                case "storeinternalid":
                    isStoreNumber = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <StoreInternalID> - isStoreNumber: " + isStoreNumber);
                    break;
                case "offer":
                    isOffer = true;
                    offer = new Offer();
                    retailEvent.getOffers().add(offer);
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <Offer> - isOffer: " + isOffer + " - offers: " + retailEvent.getOffers().size());
                    break;
                case "categorycode":
                    isCategoryCode = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <CategoryCode> - isCategoryCode: " + isCategoryCode);
                    break;
                case "activationstatuscode":
                    isActivationStatusCode = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <ActivationStatusCode> - isActivationStatusCode: " + isActivationStatusCode);
                    break;
                case "retailincentiveofferdiscounttypecode":
                    isRetailIncentiveOfferDiscountTypeCode = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <RetailIncentiveOfferDiscountTypeCode> - isRetailIncentiveOfferDiscountTypeCode: " + isRetailIncentiveOfferDiscountTypeCode);
                    break;
                case "andorbusinessruleexpressiontypecode":
                    isAndOrGet = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <AndOrBusinessRuleExpressionTypeCode> - isAndOrGet: " + isAndOrGet);
                    break;
                case "priceamount":
                case "discountpercent":
                case "discountamount":
                    isPriceAmt = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <"+theTag+"> - isPriceAmt: " + isPriceAmt);
                    break;
                case "standardid":
                    isStandardID = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <StandardID> - isStandardID: " + isStandardID);
                    break;
                case "seasonyearid":
                    isSeasonYearId = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <SeasonYearID> - isSeasonYearId: " + isSeasonYearId);
                    break;
                case "productgroup":
                    isProductGroup = true;
                    productGroup = new ProductGroup();
                    if (isBuy) {
                        offer.getBuy().getProductGroups().add(productGroup);
                    } else if (isGet) {
                        offer.getGet().getProductGroups().add(productGroup);
                    }
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <ProductGroup> - broductGroup: " + productGroup);
                    break;
                case "buy":
                    isBuy = true;
                    offer.setBuy(new Buy());
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <Buy> ");
                    break;
                case "get":
                    isGet = true;
                    offer.setGet(new Get());
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <Get> ");
                    break;
                case "requirementminimumamount":
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <RequirementMinimumAmount> ");
                    isRequirementMinimumAmount = true;
                    break;
                case "requirementquantitydecimalvalue":
                    isRequirementQuantityDecimalValue = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <RequirementQuantityDecimalValue> ");
                    break;
                case "grantedquantitylowerboundarydecimalvalue":
                    isGrantedQuantityLowerBoundaryDecimalValue = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <GrantedQuantityLowerBoundaryDecimalValue> ");
                    break;
                case "retailbonusbuyproductselectionconditioncode":
                    isRetailBonusBuyProductSelectionConditionCode = true;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <RetailBonusBuyProductSelectionConditionCode> ");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("ENTER endElement  </" + qName+">");
        try {
            switch (qName.trim().toLowerCase()){
                case "retailevent":
                    isRetailEvent = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end RetailEvent - isRetailEvent: " + isRetailEvent + " - offers: " + retailEvent.getOffers().size());
                    break;
                case "typecode":
                    isTypeCode = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("begin <TypeCode> - isTypeCode: " + isTypeCode);
                    break;
                case "salesperiod":
                    isSalesPeriod = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end SalesPeriod - isSalesPeriod: " + isSalesPeriod);
                    break;
                case "salesarea":
                    isSalesArea = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end SalesArea - isSalesArea: " + isSalesArea);
                    break;
                case "offer":
                    isOffer = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end Offer ");
                    break;
                case "productgroup":
                    isProductGroup = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end <ProductGroup> - isProductGroup: " + isProductGroup);
                    if (isBuy) {
                        Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("Buy groups: " + offer.getBuy().getProductGroups().size());
                    } else if (isGet) {
                        Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("Get groups: " + offer.getGet().getProductGroups().size());
                    }
                    break;
                case "buy":
                    isBuy = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end <Buy> - isBuy: " + isBuy);
                    break;
                case "get":
                    isGet = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end <Get> - isGet: " + isGet);
                    break;
                case "requirementminimumamount":
                    isRequirementMinimumAmount = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end <RequirementMinimumAmount>");
                    break;
                case "requirementquantitydecimalvalue":
                    isRequirementQuantityDecimalValue = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end <RequirementQuantityDecimalValue> ");
                    break;
                case "grantedquantitylowerboundarydecimalvalue":
                    isGrantedQuantityLowerBoundaryDecimalValue = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end <GrantedQuantityLowerBoundaryDecimalValue> ");
                    break;
                case "retailbonusbuyproductselectionconditioncode":
                    isRetailBonusBuyProductSelectionConditionCode = false;
                    Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("end <RetailBonusBuyProductSelectionConditionCode> ");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if (!isRetailEvent){
            Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("RetailEvent ended or not started.");
        }
        String tagValue = "";

        try {
            tagValue = new String(ch, start, length);
            if (tagValue.trim().length() == 0){
                return;
            }
        } catch (Exception e){
            Log4JUtil.getLogger(SapPromoSaxHandler.class).debug("tagValue exception. e: " + e.getMessage());
            return;
        }

        try {
            if (isAndOrGet) {
                if (isBuy) {
                    offer.getBuy().setAndOrBusinessRuleExpressionTypeCode(tagValue);
                } else if (isGet) {
                    offer.getGet().setAndOrBusinessRuleExpressionTypeCode(tagValue);
                }
                isAndOrGet = false;
            }

            if (isTypeCode && !isSalesArea && !isOffer) {
                retailEvent.setTypeCode(tagValue);
                isTypeCode = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</TypeCode>  --USEFUL_TAG--");
            }

            if (isId && !isSalesArea && !isOffer){
                retailEvent.setId(tagValue);
                isId = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</ID>  --USEFUL_TAG--");
            }

            if (isStartDate && !isSalesArea && !isOffer){
                retailEvent.getSalesPeriod().setStartDate(tagValue.replace("-", ""));
                isStartDate = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</StartDate>  --USEFUL_TAG--");
            }

            if (isEndDate && !isSalesArea && !isOffer){
                retailEvent.getSalesPeriod().setEndDate(tagValue.replace("-", ""));
                isEndDate = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</EndDate>  --USEFUL_TAG--");
            }

            if (isDescription){
                String value = tagValue.replace("," , " ");
                if (isOffer) {
                    offer.setDescription(value);
                } else {
                    retailEvent.setDescription(value);
                }
                isDescription = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + value + "</Description>  --USEFUL_TAG--");
            }

            if (isStoreNumber){
                if (isOffer) {
                    offer.setStore(tagValue);
                } else {
                    retailEvent.setStoreInternalID(tagValue);
                }
                isStoreNumber = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</StoreInternalID>  --USEFUL_TAG--");
            }

            if (isCategoryCode){
                offer.setCategoryCode(tagValue);
                isCategoryCode = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</CategoryCode>  --USEFUL_TAG--");
            }

            if (isActivationStatusCode){
                offer.setActivationStatus(tagValue);
                isActivationStatusCode = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</ActivationStatusCode>  --USEFUL_TAG--");
            }

            if (isRetailIncentiveOfferDiscountTypeCode){
                if (!isProductGroup) {
                    offer.getGet().setRetailIncentiveOfferDiscountTypeCode(tagValue);
                } else {
                    productGroup.setRetailIncentiveOfferDiscountTypeCode(tagValue);
                }
                isRetailIncentiveOfferDiscountTypeCode = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue +"</RetailIncentiveOfferDiscountTypeCode>  --USEFUL_TAG--");
            }

            if (isPriceAmt){
                int multiplier = 100;
                if (theTag.equals("PriceAmount") || theTag.equals("DiscountAmount")) {
                    multiplier = Misc.getInstance().getMultiplier();
                }
                long amount = Math.round(Double.parseDouble(tagValue) * multiplier);
                if (!isProductGroup) {
                    offer.getGet().setAmount(String.valueOf(amount));
                } else {
                    productGroup.setAmount(String.valueOf(amount));
                }
                isPriceAmt = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + String.valueOf(amount) + "</"+theTag+">  --USEFUL_TAG--");
            }

            if (isStandardID){
                currentProduct = new Product(tagValue);
                productGroup.getProducts().add(currentProduct);
                isStandardID = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</StandardID>  --USEFUL_TAG--");
            }

            if (isSeasonYearId) {
                currentProduct.setScale("X".equals(tagValue));
                isSeasonYearId = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</SeasonYearID>  --USEFUL_TAG--");
            }

            if (isRequirementMinimumAmount) {
                long requiredAmount = Math.round(Double.parseDouble(tagValue) * Misc.getInstance().getMultiplier());
                offer.getBuy().setRequirementMinimumAmount(String.valueOf(requiredAmount));
                isRequirementMinimumAmount = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "<RequirementMinimumAmount>  --USEFUL_TAG--");
            }

            if (isRequirementQuantityDecimalValue) {
                long requirementQuantityDecimalValue = Math.round(Double.parseDouble(tagValue));
                productGroup.setRequirementQuantityDecimalValue(String.valueOf(requirementQuantityDecimalValue));
                isRequirementQuantityDecimalValue = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</RequirementQuantityDecimalValue>  --USEFUL_TAG--");
            }

            if (isGrantedQuantityLowerBoundaryDecimalValue) {
                isGrantedQuantityLowerBoundaryDecimalValue = false;
                long rewarddQtyGet = Math.round(Double.parseDouble(tagValue));
                productGroup.setGrantedQuantityLowerBoundaryDecimalValue(String.valueOf(rewarddQtyGet));
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</GrantedQuantityLowerBoundaryDecimalValue>  --USEFUL_TAG--");
            }

            if (isRetailBonusBuyProductSelectionConditionCode) {
                productGroup.setRetailBonusBuyProductSelectionConditionCode(tagValue);
                isRetailBonusBuyProductSelectionConditionCode = false;
                Log4JUtil.getLogger(SapPromoSaxHandler.class).debug(theTag + tagValue + "</RetailBonusBuyProductSelectionConditionCode>  --USEFUL_TAG--");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public RetailEvent getRetailEvent() {
        return retailEvent;
    }
}
