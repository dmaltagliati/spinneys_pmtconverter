package com.ncr.promotions.handlers;

import com.ncr.promotions.Log4JUtil;
import com.ncr.promotions.Misc;
import com.ncr.promotions.entities.PromoField;
import com.ncr.promotions.entities.PromoType;
import com.ncr.promotions.entities.Promotion;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 * Created by User on 18/01/2018.
 */
public class MySaxHandler extends DefaultHandler{


    public MySaxHandler(String fileName){

        if (fileName.startsWith("ZGB1") || fileName.startsWith("ZGB01")) {
            member = "0";
            Log4JUtil.getLogger(MySaxHandler.class).info("ALL CUSTOMERS PROMO");
        }
        else {
            Log4JUtil.getLogger(MySaxHandler.class).info("ALL MEMBERS PROMO");
            member = "65535";
        }
    }

    Promotion promotion = new Promotion();
    Promotion offer;

    String member = "";
    boolean startEvent = false;
    boolean bID = false;
    boolean bSalesPeriod = false;
    boolean bStartDate = false;
    boolean bEndDate = false;
    boolean bSalesArea = false;
    boolean bDescription = false;
    boolean bStoreNumber = false;
    boolean bOffer = false;
    boolean bCategoryCode, bActivationStatusCode, bRetailOfferTypeCode, bPriceAmt, bStandardID, bProductGroup;
    boolean bMinAmount, bGet, bBuy, bMinQtyRequired, bQtyRewardRequired;
    boolean bTypeCode = false;
    boolean bAndOrGet = false;

    int offerCnt = 0, productGroup = 0;
    int requiredQtyBuy = 0, rewarddQtyGet = 0;
    int requiredAmount = 0;

    String elementID = "";
    String rewDescription = "";
    String promoID = "";
    String theTag = "";
    String typeCode = "";
    String andOrGet = "";

    private PromoType promoType;
    public Promotion getPromotion() {
        return promotion;
    }

    private String choosePromoType(int productGroup){
        String type = "";
        switch (promoType){
            case MealDeal:
                type = "MealDeal";
                break;
            case PriceOffs:
                type = "PriceOffs";
                break;
            case GenericDiscount:
                type = "GenericDiscount";
                break;
            case SpendXGetYDiscount:
                type = "SpendXGetYDiscount";
                break;
        }

        return type;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrib) throws SAXException {

        Log4JUtil.getLogger(MySaxHandler.class).debug("");
        Log4JUtil.getLogger(MySaxHandler.class).debug("ENTER startElement  <" + qName+">");
        theTag = qName.trim();
        switch (qName.trim()){
            case "RetailEvent":
                startEvent = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin RetailEvent - startEvent: " + startEvent);
                break;
            case "ID":
                if (startEvent) {
                    bID = true;
                    Log4JUtil.getLogger(MySaxHandler.class).debug("begin ID - " + bID);
                }
                if (!bID){
                    Log4JUtil.getLogger(MySaxHandler.class).debug("not promotion ID field");
                }

                break;
            case "TypeCode":
                bTypeCode = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <TypeCode> - bTypeCode: " + bTypeCode);
                break;
            case "SalesPeriod":
                bSalesPeriod = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <SalesPeriod> - bSalesPeriod: " + bSalesPeriod);
                break;
            case "StartDate":
                bStartDate = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <SalesPeriod> - bStartDate: " + bStartDate);
                break;
            case "EndDate":
                bEndDate = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <SalesPeriod> - bEndDate: " + bEndDate);
                break;
            case "SalesArea":
                bSalesArea = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <SalesArea> - bEndDate: " + bSalesArea);
                break;
            case "Description":
                bDescription = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <Description> - bDescription: " + bDescription);
                break;
            case "StoreInternalID":
                bStoreNumber = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <StoreInternalID> - bStoreNumber: " + bStoreNumber);
                break;
            case "Offer":
                bOffer = true;
                offerCnt++;
                offer = new Promotion();
                offer.setMember(member);
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <Offer> - bOffer: " + bOffer + " - offerCnt: " + offerCnt);
                break;

            case "CategoryCode":
                bCategoryCode = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <CategoryCode> - bCategoryCode: " + bCategoryCode);
                break;

            case "ActivationStatusCode":
                bActivationStatusCode = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <ActivationStatusCode> - bActivationStatusCode: " + bActivationStatusCode);
                break;

            case "RetailIncentiveOfferDiscountTypeCode":
                bRetailOfferTypeCode = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <RetailIncentiveOfferDiscountTypeCode> - bRetailOfferTypeCode: " + bRetailOfferTypeCode);
                break;
            case "AndOrBusinessRuleExpressionTypeCode":
                bAndOrGet = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <AndOrBusinessRuleExpressionTypeCode> - bAndOrGet: " + bAndOrGet);
                break;

            case "PriceAmount":
            case "DiscountPercent":
                bPriceAmt = true;

                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <"+theTag+"> - bPriceAmt: " + bPriceAmt);
                break;

            case "StandardID":
                bStandardID = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <StandardID> - bStandardID: " + bStandardID);
                break;
            case "ProductGroup":
                productGroup ++;
                bProductGroup = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <ProductGroup> - broductGroup: " + productGroup);
                break;
            case "Buy":
                bBuy = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <Buy> ");
                break;
            case "Get":
                bGet = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <Get> ");
                break;
            case "RequirementMinimumAmount":
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <RequirementMinimumAmount> ");
                bMinAmount = true;
                break;
            case "RequirementQuantityDecimalValue":
                bMinQtyRequired = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <RequirementQuantityDecimalValue> ");
                break;
            case "GrantedQuantityLowerBoundaryDecimalValue":
                bQtyRewardRequired = true;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <GrantedQuantityLowerBoundaryDecimalValue> ");
                break;
        }
    }

    int buy, get;
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        Log4JUtil.getLogger(MySaxHandler.class).debug("ENTER endElement  </" + qName+">");
        switch (qName.trim()){
            case "RetailEvent":
                startEvent = false;
                //if (buy == 1 && get == 1){
                if (typeCode.startsWith("0") || typeCode.startsWith("1")) {
                    promoType = PromoType.PriceOffs;
                }
                if ("O".equals(andOrGet)) {
                    promoType = PromoType.GenericDiscount;
                }
                if (requiredQtyBuy > 1){
                    promoType = PromoType.BuyNItemsGetDiscountOnM;
                }
                if (requiredAmount > 0) {
                    promoType = PromoType.SpendXGetYDiscount;
                }
                Log4JUtil.getLogger(MySaxHandler.class).info("I hope this is the promo: " + promoType.name());
                promotion.setType(promoType);
                promotion.getFields().add(new PromoField("OFFERS", String.valueOf(offerCnt)));
                promotion.setMember(member);

                Log4JUtil.getLogger(MySaxHandler.class).debug("end RetailEvent - startEvent: " + startEvent + " - offerCnt: " + offerCnt);
                break;
            case "TypeCode":
                bTypeCode = false;
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <TypeCode> - bTypeCode: " + bTypeCode);
                break;
            case "SalesPeriod":
                bSalesPeriod = false;
                Log4JUtil.getLogger(MySaxHandler.class).debug("end SalesPeriod - start_SalesPeriod: " + bSalesPeriod);
                break;
            case "SalesArea":
                bSalesArea = false;
                Log4JUtil.getLogger(MySaxHandler.class).debug("end SalesArea - bSalesArea: " + bSalesArea);
                break;
            case "Offer":
                offer.getFields().add(new PromoField("ID", promoID));
                offer.getFields().add(new PromoField("StandardID", elementID));
                offer.getFields().add(new PromoField("Description", rewDescription));
                promoType = PromoType.MealDeal;
                productGroup = 0;
                promotion.getFields().add(new PromoField("Offer", offer));

                bOffer = false;
                elementID = "";
                Log4JUtil.getLogger(MySaxHandler.class).debug("end Offer ");
                break;
            case "ProductGroup":
                if (bBuy && requiredQtyBuy > 1){ //penso che sia una 'BonusBuy_N_pack_get_m_pack'. carico ean per la condizione.
                    offer.getFields().add(new PromoField("StandardIDBuy", elementID));
                }

                if (bGet && rewarddQtyGet > 0){ //penso che sia una 'BonusBuy_N_pack_get_m_pack'. carico ean per la reward.
                    offer.getFields().add(new PromoField("StandardIDGet", elementID));
                }

                if (productGroup>1) {
                    offer.getFields().add(new PromoField("StandardID" + productGroup, elementID));
                }
                else
                    offer.getFields().add(new PromoField("StandardID", elementID));
                elementID = "";
                bProductGroup = false;
                Log4JUtil.getLogger(MySaxHandler.class).debug("end <ProductGroup> - broductGroup: " + productGroup);
                break;
            case "Buy":
                buy++; //conto le buy e le get, se entrambi sono == 1 presumo che sia un GenericDiscount
                Log4JUtil.getLogger(MySaxHandler.class).debug("end <Buy> - buy: " + buy);
                break;
            case "Get":
                get++;//conto le buy e le get, se entrambi sono == 1 presumo che sia un GenericDiscount
                Log4JUtil.getLogger(MySaxHandler.class).debug("end <Get> - get: " + get);
                break;
            case "RequirementMinimumAmount":
                Log4JUtil.getLogger(MySaxHandler.class).debug("end <RequirementMinimumAmount>");
                break;
            case "RequirementQuantityDecimalValue":
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <RequirementQuantityDecimalValue> ");
                break;
            case "GrantedQuantityLowerBoundaryDecimalValue":
                Log4JUtil.getLogger(MySaxHandler.class).debug("begin <GrantedQuantityLowerBoundaryDecimalValue> ");
                break;

        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        //Log4JUtil.getLogger(MySaxHandler.class).debug("ENTER characters.");
        if (!startEvent){
            Log4JUtil.getLogger(MySaxHandler.class).debug("RetailEvent ended or not started.");
        }

        String tagValue = "";
        //String theTag = "";

        try {
            tagValue = new String(ch, start, length);
            if (tagValue.trim().length() == 0){
                return;
            }
        }catch (Exception e){
            Log4JUtil.getLogger(MySaxHandler.class).debug("tagValue exception. e: " + e.getMessage());
            return;
        }

        if (bAndOrGet && bGet) {
            andOrGet = tagValue;
            bAndOrGet = false;
        }
        if (bTypeCode && !bSalesArea && !bOffer) {
            typeCode = tagValue;
            bTypeCode = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</TypeCode>  --USEFUL_TAG--");
        }
        if (bID && !bSalesArea && !bOffer){
            //theTag = "<ID>";
            promoID = tagValue;
            promotion.getFields().add(new PromoField(theTag, tagValue));

            bID = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</ID>  --USEFUL_TAG--");
        }
        if (bStartDate && !bSalesArea && !bOffer){
            //theTag = "<StartDate>";
            promotion.getFields().add(new PromoField(theTag, tagValue.replace("-", "")));

            bStartDate = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</StartDate>  --USEFUL_TAG--");
        }
        if (bEndDate && !bSalesArea && !bOffer){
            //theTag = "<EndDate>";
            promotion.getFields().add(new PromoField(theTag, tagValue.replace("-", "")));

            bEndDate = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</EndDate>  --USEFUL_TAG--");
        }

        if (bDescription){
            //theTag = "<Description>";
            String value = tagValue.replace("," , " ");
            promotion.getFields().add(new PromoField(theTag, value));

            rewDescription = value;
            bDescription = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + value + "</Description>  --USEFUL_TAG--");
        }

        if (bStoreNumber){
            //theTag = "<StoreInternalID>";
            promotion.getFields().add(new PromoField(theTag, tagValue));

            bStoreNumber = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</StoreInternalID>  --USEFUL_TAG--");
        }

        if (bCategoryCode){
            //theTag = "<CategoryCode>";
            offer.getFields().add(new PromoField(theTag, tagValue));
            bCategoryCode = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</CategoryCode>  --USEFUL_TAG--");
        }

        if (bActivationStatusCode){
            //theTag = "<ActivationStatusCode>";
            offer.getFields().add(new PromoField(theTag, tagValue));
            bActivationStatusCode = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</ActivationStatusCode>  --USEFUL_TAG--");
        }

        if (bRetailOfferTypeCode){
            //theTag = "<RetailIncentiveOfferDiscountTypeCode>";
            offer.getFields().add(new PromoField(theTag, tagValue));

            bRetailOfferTypeCode = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue +"</RetailIncentiveOfferDiscountTypeCode>  --USEFUL_TAG--");
        }

        if (bPriceAmt){
            double amount = 0;
            if (theTag.equals("PriceAmount")) {
                amount = Double.parseDouble(tagValue) * Misc.getInstance().getMultiplier();
            }
            else
            {
                amount = Double.parseDouble(tagValue) * 100;
            }

            offer.getFields().add(new PromoField(theTag, String.valueOf(amount)));
            promotion.getFields().add(new PromoField(theTag, String.valueOf(amount)));
            bPriceAmt = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + String.valueOf(amount) + "</"+theTag+">  --USEFUL_TAG--");
        }

        if (bStandardID){
            if (productGroup > 1)
                //theTag = "<StandardID"+productGroup+">";
                theTag += productGroup;
                //else
                //theTag = "<StandardID>";
            //promotion.getFields().add(new PromoField(theTag, tagValue));
            elementID += tagValue + ";";

            bStandardID = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</StandardID>  --USEFUL_TAG--");
        }

        if (bMinAmount){
            requiredAmount = (int)Double.parseDouble(tagValue) * Misc.getInstance().getMultiplier();
            promotion.getFields().add(new PromoField(theTag, String.valueOf(requiredAmount)));
            offer.getFields().add(new PromoField(theTag, String.valueOf(requiredAmount)));

            bMinAmount = false;
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "<RequirementMinimumAmount>  --USEFUL_TAG--");
        }
        if (bMinQtyRequired){
            promotion.getFields().add(new PromoField(theTag, tagValue));
            offer.getFields().add(new PromoField(theTag, tagValue));
            bMinQtyRequired = false;
            requiredQtyBuy = (int)Double.parseDouble(tagValue);
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</RequirementQuantityDecimalValue>  --USEFUL_TAG--");
        }
        if (bQtyRewardRequired){
            promotion.getFields().add(new PromoField(theTag, tagValue));
            offer.getFields().add(new PromoField(theTag, tagValue));
            bQtyRewardRequired = false;
            rewarddQtyGet = (int)Double.parseDouble(tagValue);
            Log4JUtil.getLogger(MySaxHandler.class).debug(theTag + tagValue + "</GrantedQuantityLowerBoundaryDecimalValue>  --USEFUL_TAG--");
        }
    }
}
