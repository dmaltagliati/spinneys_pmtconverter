package com.ncr.promotions;

/**
 * Created by User on 21/02/2018.
 */
public class Association {
    String item, promovar;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getPromovar() {
        return promovar;
    }

    public void setPromovar(String promovar) {
        this.promovar = promovar;
    }
}
