package com.ncr.promotions;

import com.google.gson.Gson;
import com.ncr.promotions.entities.Promotion;
import com.ncr.promotions.entities.RetailEvent;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class Misc {
    private static Misc instance;
    public static String PATH_CONF = "config";
    private String incomingPath = "";
    private String templatePath = "";
    private String errorPath = "";
    private String processedPath = "";
    private String pmtPath = "";
    private String jsonPath = "";
    private String store = "";
    private int multiplier = 1;
    private String currentXmlFile = "";
    private int maxProducts = 500;
    private boolean userDefinedSortKeyByPrice = false;
    private String scaleProductDescription = "";
    private String discountLimitValue = "0";
    private String discountLimitPromovar = "0";

    public static Misc getInstance() {
        if (instance == null)
            instance = new Misc();

        return instance;
    }

    public boolean loadMainParameters() {
        Properties paramProperties = new Properties();
        boolean success = true;
        try {
            paramProperties.load(new FileInputStream(PATH_CONF + File.separatorChar + Version.getName() + ".properties"));
            incomingPath = paramProperties.getProperty("incomingPath");
            templatePath = paramProperties.getProperty("templatePath");
            errorPath = paramProperties.getProperty("errorPath");
            processedPath = paramProperties.getProperty("processedPath");
            pmtPath = paramProperties.getProperty("pmtPath");
            store = paramProperties.getProperty("store");
            jsonPath = paramProperties.getProperty("jsonPath");
            maxProducts = Integer.parseInt(paramProperties.getProperty("maxProducts"));
            int decimalCurrency = Integer.parseInt(paramProperties.getProperty("decimalCurrency"));
            userDefinedSortKeyByPrice = paramProperties.getProperty("userDefinedSortKeyByPrice").toLowerCase().equals("true");
            scaleProductDescription = paramProperties.getProperty("scaleProductDescription");
            discountLimitValue = paramProperties.getProperty("discountLimitValue");
            discountLimitPromovar = paramProperties.getProperty("discountLimitPromovar");

            for (int i = 0; i < decimalCurrency; i++)
                multiplier *= 10;

            Log4JUtil.getLogger(this.getClass()).info("*** READING MAIN PARAMETERS *** - BEG -");
            Log4JUtil.getLogger(this.getClass()).info("incomingPath              : " + incomingPath);
            Log4JUtil.getLogger(this.getClass()).info("templatePath              : " + templatePath);
            Log4JUtil.getLogger(this.getClass()).info("errorPath                 : " + errorPath);
            Log4JUtil.getLogger(this.getClass()).info("processedPath             : " + processedPath);
            Log4JUtil.getLogger(this.getClass()).info("pmtPath                   : " + pmtPath);
            Log4JUtil.getLogger(this.getClass()).info("jsonPath                  : " + jsonPath);
            Log4JUtil.getLogger(this.getClass()).info("store                     : " + store);
            Log4JUtil.getLogger(this.getClass()).info("decimalCurrency           : " + decimalCurrency);
            Log4JUtil.getLogger(this.getClass()).info("multiplier                : " + multiplier);
            Log4JUtil.getLogger(this.getClass()).info("maxProducts               : " + maxProducts);
            Log4JUtil.getLogger(this.getClass()).info("userDefinedSortKeyByPrice : " + userDefinedSortKeyByPrice);
            Log4JUtil.getLogger(this.getClass()).info("scaleProductDescription   : " + scaleProductDescription);
            Log4JUtil.getLogger(this.getClass()).info("discountLimitValue        : " + discountLimitValue);
            Log4JUtil.getLogger(this.getClass()).info("discountLimitPromovar     : " + discountLimitPromovar);
            Log4JUtil.getLogger(this.getClass()).info("*** READING MAIN PARAMETERS *** - END -");

        } catch (Exception e) {
            success = false;
            Log4JUtil.getLogger(this.getClass()).fatal(e.getMessage());
        }
        return success;
    }

    public boolean isUserDefinedSortKeyByPrice() {
        return userDefinedSortKeyByPrice;
    }

    public String getIncomingPath() {
        return incomingPath;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public String getErrorPath() {
        return errorPath;
    }

    public String getProcessedPath() {
        return processedPath;
    }

    public String getPmtPath() {
        return pmtPath;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public String getStore() {
        return store;
    }

    public int getMaxProducts() {
        return maxProducts;
    }

    public String getScaleProductDescription() {
        return scaleProductDescription;
    }

    public String getDiscountLimitValue() {
        return discountLimitValue;
    }

    public String getDiscountLimitPromovar() {
        return discountLimitPromovar;
    }

    public void fileMove(String source, String dest) {
        Log4JUtil.getLogger(this.getClass()).info("ENTER");
        Log4JUtil.getLogger(this.getClass()).info("moving file >" + source + "< in >" + dest + "<");
        InputStream inStream = null;
        OutputStream outStream = null;

        try {

            File afile = new File(source);
            File bfile = new File(dest);

            inStream = new FileInputStream(afile);
            outStream = new FileOutputStream(bfile);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }


            inStream.close();
            outStream.close();

            //delete the original file

            String fileSource = afile.getPath();
            if (afile.delete())
                Log4JUtil.getLogger(this.getClass()).info(fileSource + " deleted successful!");
            else
                Log4JUtil.getLogger(this.getClass()).info(fileSource + " NOT DELETED!");

            System.out.println("File is copied successful!");
            Log4JUtil.getLogger(this.getClass()).info("File is copied successful!");

        } catch (Exception e) {
            Log4JUtil.getLogger(this.getClass()).error("File move failed... e: " + e);
        }
        Log4JUtil.getLogger(this.getClass()).info("EXIT");
    }

    public String getCurrentXmlFile() {
        return currentXmlFile;
    }

    public void setCurrentXmlFile(String currentXmlFile) {
        this.currentXmlFile = currentXmlFile;
    }

    String wholePmt = "";

    public String getWholePmt() {
        return wholePmt;
    }

    public void setWholePmt(String wholePmt) {
        this.wholePmt += wholePmt;
    }

    private List<Association> associationList = new ArrayList<>();

    public List<Association> getAssociationList() {
        return associationList;
    }

    public void createJson(RetailEvent re, String filename){
        PromoJson pj = new PromoJson();
        OutputStream jFileOutStream = null;
        File jsonFile = new File("Promotion_" +     filename + ".json");
        Association association = new Association();

        pj.setPromoNumber(re.getId());
        pj.setStartDate(re.getSalesPeriod().getStartDate());
        pj.setEndDate(re.getSalesPeriod().getEndDate());
        pj.setPmt(getWholePmt());

        Iterator<Association> associationIterator = associationList.iterator();
        while (associationIterator.hasNext()){
            association = associationIterator.next();
            pj.getAssociations().add(association);
        }

        List<PromoJson>  listPJ = new ArrayList<>();
        listPJ.add(pj);
        Gson gson = new Gson();
        String jsonString = gson.toJson(listPJ);
        System.out.println(jsonString);
        Log4JUtil.getLogger(this.getClass()).info("creating json :>" + jsonString + "<");

        try {
            jFileOutStream = new FileOutputStream(jsonPath + File.separatorChar + jsonFile);
            jFileOutStream.write(jsonString.getBytes());
            jFileOutStream.close();

        }catch (Exception e){

        }



    }
}
